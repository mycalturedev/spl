<?php


namespace Mycalture\Spl;


use Mycalture\Spl\Console\Commands\UpdateRetailPrice;

class Application
{
    public function commands()
    {
        return [
            new UpdateRetailPrice()
        ];
    }
}