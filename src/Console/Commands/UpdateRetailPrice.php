<?php


namespace Mycalture\Spl\Console\Commands;


use Illuminate\Database\Capsule\Manager as Capsule;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Dotenv\Dotenv;

class UpdateRetailPrice extends Command
{
    protected function configure()
    {
        $this->setName('update:retail')
            ->setDescription('Update the retail price based on the UPC')
            ->addArgument('csv', InputArgument::REQUIRED, 'The CSV file containing the UPC and updated retail price.')
            ->addOption('upc-col', 'U', InputOption::VALUE_OPTIONAL, 'The column number containing the UPC', 0)
            ->addOption('price-col', 'P', InputOption::VALUE_OPTIONAL, 'The column number containing the updated price', 1)
            ->addOption('delimiter', 'D', InputOption::VALUE_OPTIONAL, 'The CSV delimiter character', ',');


        $dotenv = new Dotenv();
        $dotenv->load(__DIR__.'/../../../.env');

        $capsule = new Capsule();
        $capsule->addConnection([
            'driver' => 'mysql',
            'host' => getenv('DATABASE_SERVER'),
            'database' => getenv('DATABASE_SCHEMA'),
            'username' => getenv('DATABASE_USERNAME'),
            'password' => getenv('DATABASE_PASSWORD'),
        ]);

        $capsule->setAsGlobal();
    }

    protected function updateDatabase($upc, $price)
    {
        $product = Capsule::table('products')->where('code', '=', $upc)->update([
            'price' => $price,
        ]);
    }

    protected function processCsv($path, $upcCol, $priceCol, $delimiter, OutputInterface $output)
    {
        $csv = Reader::createFromPath($path);
        $csv->setDelimiter($delimiter);

        $statement = new Statement();
        foreach ($statement->process($csv) as $row) {
            $upc = $row[$upcCol];
            $price = $row[$priceCol];

            if (is_numeric($upc)) {
                $price = str_replace(',', '.', $price);
                $output->write($upc . ': ' . $price);

                $this->updateDatabase($upc, $price);

                $output->writeln("\t\t" . '<info>OK</info>');
            }
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $csvPath = realpath($input->getArgument('csv'));

        if (!file_exists($csvPath)) {
            $output->writeln('<error>The CSV file could not be found at ' . $csvPath . '</error>');
            exit(1);
        }

        $this->processCsv($csvPath,
            $input->getOption('upc-col'),
            $input->getOption('price-col'),
            $input->getOption('delimiter'),
            $output
        );
    }
}